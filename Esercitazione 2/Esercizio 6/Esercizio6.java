import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class Esercizio6
{
    public static void main(String[] args)
    {
        XMLReader xmlr = new XMLReader();
        xmlr.Start();
    }
}

class XMLReader
{
    // Membri
    DocumentBuilderFactory _DOCFactory  = null;
    DocumentBuilder _DOCBuilder         = null;
    Document _DOC                       = null;
    
    public XMLReader()
    {
        try
        {    
            _DOCFactory = DocumentBuilderFactory.newInstance();
            _DOCBuilder = _DOCFactory.newDocumentBuilder();
            _DOC        = _DOCBuilder.parse("doc06.xml");
        }
        catch(Exception e) 
        {
            System.out.println (e);
        }
    }
    
    public void Start()
    {
		System.out.println ("------------------------------------------------------------\n");
        GetDocData (_DOC);
        System.out.println ("\n------------------------------------------------------------");
    }
    
    private String GetDocType (Node nd) 
    {
		switch(nd.getNodeType())
        {
            case Node.DOCUMENT_NODE:
                return "DOCUMENT_NODE";
            
            case Node.DOCUMENT_TYPE_NODE:
                return "DOCUMENT_TYPE_NODE";
                
            case Node.ELEMENT_NODE:
                return "ELEMENT_NODE";
		}
		
		return "WARNING ->  UNEXPECTED( " + nd.getNodeType() + " )!";
	}

	private void GetNodeInfo (String nodeInfo, Node nd) 
    {
		String type     = GetDocType (nd);
		String name     = nd.getNodeName();
		String value    = nd.getNodeValue();
		
		System.out.println (" ->\t" + nodeInfo + 
                            "( " + type + " ).Name( " + name + " ):  " + 
                            ((value == null) ? value : "NULL"));
	}
    
    private void GetDocData(Document _doc)
    {
		GetNodeInfo ("Document", _doc);
		
		Node doc = _doc.getFirstChild();
		GetNodeInfo ("DTD", doc);
		
		Node root = doc.getNextSibling();
		GetNodeInfo ("flights", root);
		
		Node itm1 = root.getFirstChild();
		GetNodeInfo ("flights.Item1", itm1);
		
		Node itm2 = itm1.getNextSibling();
		GetNodeInfo ("flights.Item2", itm2);
		
		Node itm3 = itm2.getNextSibling();
		GetNodeInfo ("flights.Item3", itm3);
	}
}