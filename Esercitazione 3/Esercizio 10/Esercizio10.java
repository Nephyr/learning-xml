import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;

public class Esercizio10
{
    public static void main(String[] args)
    {
        XMLReader xmlr = new XMLReader();
        xmlr.Start();
    }
}

class XMLReader
{
    // Membri
    DocumentBuilderFactory _DOCFactory  = null;
    DocumentBuilder _DOCBuilder         = null;
    Document _DOC                       = null;
    
    public XMLReader()
    {
        try
        {    
            _DOCFactory = DocumentBuilderFactory.newInstance();
            _DOCFactory.setIgnoringElementContentWhitespace(true);
            
            _DOCBuilder = _DOCFactory.newDocumentBuilder();
            _DOC        = _DOCBuilder.parse("doc10.xml");
        }
        catch(Exception e) 
        {
            System.out.println (e);
        }
    }
    
    public void Start()
    {
		System.out.println ("------------------------------------------------------------\n");
        StepAllNodes(_DOC.getElementsByTagName("CD"));
        System.out.println ("\n------------------------------------------------------------");
    }

	private void GetValue (Node nd) 
    {
        for (Node i = nd.getFirstChild(); i != null; i = i.getNextSibling())
        {               
            NamedNodeMap attributes = i.getAttributes();
            int len = attributes.getLength();
            
            if(i.getNodeName().equalsIgnoreCase("title") ||
               i.getNodeName().equalsIgnoreCase("artist"))
                System.out.println("\t\t" + i.getNodeName() + ": " + i.getFirstChild().getNodeValue());
               
            
            for ( int n = 0; n < len ; n++ )
            {
                Node attr = attributes.item( n );
                
                if(attr.getNodeName().equalsIgnoreCase("lang") ||
                   attr.getNodeName().equalsIgnoreCase("sex"))
                    System.out.println("\t\t\t" + attr.getNodeName() + ": " + attr.getNodeValue());
            }
        }
        
        System.out.print("\n");
    }
    
    private void StepAllNodes( NodeList nd )
    {
        for (int i = 0; i != nd.getLength(); i++)
        {
            NamedNodeMap titleAttr = nd.item(i).getFirstChild().getAttributes();
            Node lang = titleAttr.getNamedItem("LANG");
            
            NamedNodeMap artistAttr = nd.item(i).getFirstChild().getNextSibling().getAttributes();
            Node sex = artistAttr.getNamedItem("SEX");
            
            if ( lang != null || sex != null )                
                if ( lang.getNodeValue().equalsIgnoreCase("italian") || 
                     sex.getNodeValue().equalsIgnoreCase("female") )
                {
                    System.out.println("\t CD: ");
                    GetValue( nd.item(i) );
                }
        }
    }
}