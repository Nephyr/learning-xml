import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import java.util.*;

public class Esercizio11
{
    public static void main(String[] args)
    {
        XMLReader xmlr = new XMLReader();
        xmlr.Start();
    }
}

class XMLReader
{
    // Membri
    DocumentBuilderFactory _DOCFactory  = null;
    DocumentBuilder _DOCBuilder         = null;
    Document _DOC                       = null;
    
    // Supporto
    Map<String, Float> _MapCurrency     = null;
    Map<String, Integer> _MapOccurence    = null;
    
    public XMLReader()
    {
        try
        {    
            _DOCFactory   = DocumentBuilderFactory.newInstance();
            _DOCFactory.setIgnoringElementContentWhitespace(true);
            
            _DOCBuilder   = _DOCFactory.newDocumentBuilder();
            _DOC          = _DOCBuilder.parse("doc10.xml");
        
            _MapCurrency  = new HashMap<String, Float>();
            _MapOccurence = new HashMap<String, Integer>();
        }
        catch(Exception e) 
        {
            System.out.println (e);
        }
    }
    
    public void Start()
    {
		System.out.println ("------------------------------------------------------------\n");
        StepAllNodes(_DOC.getElementsByTagName("PRICE"));
        System.out.println ("\n------------------------------------------------------------\n");
        StepAllValues();
        System.out.println ("\n------------------------------------------------------------");
    }
    
	private void GetValue (Node nd) 
    {
        for (Node i = nd; i != null; i = i.getNextSibling())
        {               
            NamedNodeMap attributes = i.getAttributes();
            int len = attributes.getLength();
            
            for ( int n = 0; n < len ; n++ )
            {
                Node attr   = attributes.item( n );
                String name = attr.getNodeValue();
                float value = Float.parseFloat(i.getFirstChild().getNodeValue());
                
                if(!_MapCurrency.containsKey(name))
                {
                    _MapCurrency.put(name, value);
                    _MapOccurence.put(name, 1);
                }
                else
                {
                    float tmp1 = _MapCurrency.get(name);
                    int tmp2   = _MapOccurence.get(name);
                    
                    _MapCurrency.put(name, tmp1 + value);
                    _MapOccurence.put(name, ++tmp2);
                }
                
                System.out.println("\t" + i.getNodeName() + ": " + value);
                System.out.println("\t  ->  " + attr.getNodeName() + ": " + name);
            } 
        }
        
        System.out.print("\n");
    }
    
    private void StepAllValues()
    {
        Iterator it = _MapCurrency.entrySet().iterator();
        System.out.println("\tMedia dei costi dei CD raggruppati per valuta:\n");
        
        while (it.hasNext())
        {
            Map.Entry _map  = (Map.Entry) it.next();
            String key      = (String) _map.getKey();
            float val       = (float) _map.getValue();
            int div         = _MapOccurence.get(key);

            System.out.println("\t  ->  " + key + ":\t" + (val / div));
        }
    }
    
    private void StepAllNodes( NodeList nd )
    {
        for (int i = 0; i != nd.getLength(); i++)
        {
            GetValue( nd.item(i) );
        }
    }
}