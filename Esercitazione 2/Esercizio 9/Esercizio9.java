import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class Esercizio9
{
    public static void main(String[] args)
    {
        XMLReader xmlr = new XMLReader();
        xmlr.Start();
    }
}

class XMLReader
{
    // Membri
    DocumentBuilderFactory _DOCFactory  = null;
    DocumentBuilder _DOCBuilder         = null;
    Document _DOC                       = null;
    
    public XMLReader()
    {
        try
        {    
            _DOCFactory = DocumentBuilderFactory.newInstance();
            _DOCBuilder = _DOCFactory.newDocumentBuilder();
            _DOC        = _DOCBuilder.parse("doc09.xml");
        }
        catch(Exception e) 
        {
            System.out.println (e);
        }
    }
    
    public void Start()
    {
		System.out.println ("------------------------------------------------------------\n");
		Node doc = _DOC.getFirstChild();
		Node root = doc.getNextSibling();
        StepAllFlights(root);
        System.out.println ("------------------------------------------------------------");
    }
    
    private String GetDocType (Node nd) 
    {
		switch(nd.getNodeType())
        {
            case Node.DOCUMENT_NODE:
                return "DOCUMENT_NODE";
            
            case Node.TEXT_NODE:
                return "TEXT_NODE";
            
            case Node.DOCUMENT_TYPE_NODE:
                return "DOCUMENT_TYPE_NODE";
                
            case Node.ELEMENT_NODE:
                return "ELEMENT_NODE";
		}
		
		return "WARNING ->  UNEXPECTED( " + nd.getNodeType() + " )!";
	}

	private void GetFlightValue (Node nd) 
    {
        for (Node i = nd.getFirstChild(); i != null; i = i.getNextSibling())
        {
            Node dsn = i.getFirstChild();
            System.out.println("\t  +-> " + i.getNodeName() + ":  " + ((dsn != null) ? dsn.getNodeValue() : " --"));
        }
            
        System.out.println(" ");
    }
    
    private boolean checkData(Node nd)
    {
        for (Node i = nd.getFirstChild(); i != null; i = i.getNextSibling())
            if (i.getNodeName().equals("data"))
            {
                Node dsn = i.getFirstChild();
                String tmp = ((dsn != null) ? dsn.getNodeValue() : "--");
                
                return tmp.equals("27/04/2012");
            }
        
        return false;
    }
    
    private boolean checkPartenza(Node nd)
    {
        for (Node i = nd.getFirstChild(); i != null; i = i.getNextSibling())
            if (i.getNodeName().equals("partenza"))
            {
                Node dsn = i.getFirstChild();
                String tmp = ((dsn != null) ? dsn.getNodeValue() : "--");
                
                return tmp.contains("Milano");
            }
        
        return false;
    }
    
    private void StepAllFlights( Node nd )
    {
        for (Node i = nd.getFirstChild(); i != null; i = i.getNextSibling())
            if (checkData(i) && checkPartenza(i))
            {   
                System.out.println("\t ---  " + i.getNodeName());            
                GetFlightValue(i);
            }
    }
}